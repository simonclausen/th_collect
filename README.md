# Temperature and humidity collector

This project contains tooling and instructions for collecting data from Xiaomi Mijia sensors and making it available for other systems.

## Requirements

* Raspberry Pi w/ Bluetooth
* Raspbian

## Setup

```sh
sudo apt update
sudo apt install git python3 python3-pip libglib2.0-dev
sudo pip3 install bluepy mitemp-bt
```

## Descovering bluetooth devices

`sudo  hcitool lescan`

The device MACs are usually preficed with "58:2D:34:38:03" and devices named "MJ_HT_V1".

## Acknoledgements

* `demo.py` from [https://github.com/ratcashdev/mitemp/blob/master/demo.py](https://github.com/ratcashdev/mitemp/blob/master/demo.py)
